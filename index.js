const http = require('http');
const port = 4000;
const server = http.createServer;
// /profile welcome to your profile get
// /courses here's our courses available get
// /addCourse add course to our resources post
// /updateCourse 
// /archiveCourse

server((req, res) => {
    if (req.url == "/" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to Booking System");
    }
    else if (req.url == "/profile" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to your profile ");
    }
    else if (req.url == "/courses" && req.method == "GET") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Here's our courses available");
    }
    else if (req.url == "/addCourse" && req.method == "POST") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Add a course to our resources");
    }
    else if (req.url == "/updateCourse" && req.method == "PUT") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Update a course to our resources");
    }
    else if (req.url == "/archiveCourse" && req.method == "DELETE") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Archive courses to our resources");
    }else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Page Not Found!");
    }

}).listen(port);

console.log(`Server is running at port ${port}.`);